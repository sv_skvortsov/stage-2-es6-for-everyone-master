import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal({ fighter, position }) {
  const container = createElement({ tagName: 'div', className: `modal-body-container-${position}` })
  const winnerImage = createFighterImage(fighter);
  const winnerStamp = createElement({ tagName: 'div', className: `modal-winner-stamp` });

  container.append(winnerImage, winnerStamp);
  showModal({ title: `${fighter.name} is the ...`, bodyElement: container })
}
