import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter)
    fighterElement.append(createFighterName(fighter.name), createFighterImage(fighter), createFighterDetails(fighter));
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterName(name) {
  const nameElement = createElement({
    tagName: 'h3',
    className: 'fighter-preview___name',
  });

  nameElement.innerText = name;
  return nameElement;
}

function createFighterDetails({ attack, defense, health }) {
  const detailsElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___details',
  });

  detailsElement.innerHTML = `<ul>
    <li><span>Health:</span> <span>${health}</span></li>
    <li><span>Attack:</span> <span>${attack}</span></li>
    <li><span>Defense:</span> <span>${defense}</span></li>
  </ul>`;

  return detailsElement;
}
