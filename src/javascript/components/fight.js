import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const criticalHitTimeRegeneration = 10000;
  let outsideResolve;
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;
  let pressedKeys = [];
  let isPlayerOneCriticalHitAvailable = true;
  let isPlayerTwoCriticalHitAvailable = true;
  const criticalHitBarLeftElement = document.getElementById('left-critical-hit-text');
  const criticalHitBarRightElement = document.getElementById('right-critical-hit-text');

  criticalHitBarLeftElement.style.animationDuration = `${criticalHitTimeRegeneration / 1000}s`;
  criticalHitBarRightElement.style.animationDuration = `${criticalHitTimeRegeneration / 1000}s`;


  document.addEventListener('keydown', (e) => {
    const isPlayerOneAttack = e.code === controls.PlayerOneAttack;
    const isPlayerTwoAttack = e.code === controls.PlayerTwoAttack;
    const isPlayerOneBlocking = pressedKeys.includes(controls.PlayerOneBlock);
    const isPlayerTwoBlocking = pressedKeys.includes(controls.PlayerTwoBlock);

    // Don't push same key code
    if (!pressedKeys.includes(e.code))
      pressedKeys.push(e.code);

    const isPlayerOneUseCriticalHit = controls.PlayerOneCriticalHitCombination.every(key => pressedKeys.includes(key));
    const isPlayerTwoUseCriticalHit = controls.PlayerTwoCriticalHitCombination.every(key => pressedKeys.includes(key));

    // When player one attack
    if (isPlayerOneAttack && !isPlayerOneBlocking && !isPlayerTwoBlocking) {
      secondFighterHealth -= getDamage(firstFighter, secondFighter);
      updateHealthIndicator(secondFighter, secondFighterHealth, 'right')
    }

    // When player one use critical hit
    if (isPlayerOneUseCriticalHit && isPlayerOneCriticalHitAvailable) {
      secondFighterHealth -= getCriticalHitDamage(firstFighter, secondFighter);
      isPlayerOneCriticalHitAvailable = false;
      criticalHitBarLeftElement.classList.add('critical-bar-animation-left');

      updateHealthIndicator(secondFighter, secondFighterHealth, 'right')
      setTimeout(() => {
        isPlayerOneCriticalHitAvailable = true;
        criticalHitBarLeftElement.classList.remove('critical-bar-animation-left');
      }, criticalHitTimeRegeneration);
    }

    // When player two attack
    if (isPlayerTwoAttack && !isPlayerTwoBlocking && !isPlayerOneBlocking) {
      firstFighterHealth -= getDamage(secondFighter, firstFighter);
      updateHealthIndicator(firstFighter, firstFighterHealth, 'left')
    }

    // When player two use critical hit
    if (isPlayerTwoUseCriticalHit && isPlayerTwoCriticalHitAvailable) {
      firstFighterHealth -= getCriticalHitDamage(secondFighter, firstFighter);
      isPlayerTwoCriticalHitAvailable = false;
      criticalHitBarRightElement.classList.add('critical-bar-animation-right');

      updateHealthIndicator(firstFighter, firstFighterHealth, 'left')
      setTimeout(() => {
        isPlayerTwoCriticalHitAvailable = true;
        criticalHitBarRightElement.classList.remove('critical-bar-animation-right');
      }, criticalHitTimeRegeneration);
    }

    if (firstFighterHealth <= 0)
      outsideResolve({ fighter: secondFighter, position: 'right' });

    if (secondFighterHealth <= 0)
      outsideResolve({ fighter: firstFighter, position: 'left' });
  });

  document.addEventListener('keyup', (e) => {
    pressedKeys = pressedKeys.filter(code => code !== e.code);
  });

  return new Promise((resolve) => {
    outsideResolve = resolve;
  });
}

function updateHealthIndicator({ health }, currentHealth, position) {
  const fighterIndicatorElement = document.getElementById(`${position}-fighter-indicator`);
  const healthInPercents = (currentHealth * 100) / health;

  fighterIndicatorElement.style.width = `${healthInPercents < 0 ? 0 : healthInPercents}%`;
}

function getCriticalHitDamage({ attack }, { defense }) {
  const criticalHitPower = 2 * attack;
  const damage = criticalHitPower - getBlockPower({ defense });

  return damage;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return (damage < 0 ? 0 : damage);
}

export function getHitPower({ attack }) {
  const criticalHitChance = Math.random() + 1;

  return (attack * criticalHitChance);
}

export function getBlockPower({ defense }) {
  const dodgeChance = Math.random() + 1;

  return (defense * dodgeChance);
}
